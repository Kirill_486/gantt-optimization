import * as React from 'react';
import { connect } from 'react-redux';

import { addCard } from '../store/actions/cardActions';
import { makeEmptyCardModel } from '../store/helpers';
import {IApplicationState, ICard} from '../store/types/types'
import Card from './Card';

export interface ICardsPageStateProps {
    cards: ICard[]
}

export interface ICardsPageDispatchProps {
    addCard: () => void;
}

export interface ICardsPageProps extends ICardsPageStateProps, ICardsPageDispatchProps {

}

export const CardsPage: React.SFC<ICardsPageProps> = (props) => (
    <main>
    <button onClick={props.addCard}>Add Card</button>
        <div className="cardList">
            {props.cards.map((card: ICard)=>(
                <Card key={card.id} cardId={card.id} />
            ))}
        </div>
    </main>
);

CardsPage.displayName = 'CardsPage';

// const mapStateToProps = (state: IApplicationState) => ({
//     cards: state.cards.filter((card: ICard) => typeof card.parentId === undefined)
// });
const mapStateToProps = (state: IApplicationState) => {
    const cards = state.cards.filter((card: ICard) => !card.parentId);
    return {
        cards
    }
}

const mapDispatchToProps = (dispatch: any, props: any) => ({
    addCard: () => dispatch(addCard(props.cardId, makeEmptyCardModel('newCard1', 0, 1)))
});

const connectedCardsPage =
    connect<ICardsPageStateProps, ICardsPageDispatchProps>(mapStateToProps, mapDispatchToProps)(CardsPage);

export default connectedCardsPage;
