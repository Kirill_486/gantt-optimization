import * as React from 'react';
import { connect } from 'react-redux';

import {addCard, changeNumbers, decreaseEnd, decreaseStart, increaseEnd, increaseStart, removeCard } from '../store/actions/cardActions';
import { makeEmptyCardModel } from '../store/helpers';
import {IApplicationState, ICard} from '../store/types/types';

interface IIncomingProps {
    cardId: string
}

interface IStateProps {
    card?: ICard;
    subcards?: ICard[]
}

interface IDispatchProps {
    increaseStart?: () => void;
    increaseEnd?: () => void;
    decreaseStart?: () => void;
    decreaseEnd?: () => void;

    removeCard?: () => void;
    addSubcard?: () => void;
}

interface ICardProps extends IIncomingProps, IStateProps, IDispatchProps {

}

interface ICardState extends ICard {

}

export class Card extends React.Component<ICardProps, ICardState> {

    // private hasCard: boolean;
    private isEpic: boolean;

    constructor(props: ICardProps) {
        super(props);

        this.isEpic = this.props.subcards.length > 0;

    }

    render() {
        this.isEpic = this.props.subcards.length > 0;
        console.log(`Card ${this.props.cardId} epic -  ${this.isEpic}`);

        const startNums = this.props.subcards.map( (item)=> item.startNum);
        const endNums = this.props.subcards.map( (item)=> item.endNum);

        return (
            <div className="card" id={this.props.cardId}>
                <div>
                    <span>{this.props.card.title}</span>
                </div>

                {this.isEpic && (
                    <div className="card__epic">{`${Math.min.apply(null, startNums)} - ${Math.max.apply(null, endNums)}`}</div>
                )}

                <div className="card__buttons">
                    <button onClick={this.props.addSubcard}>Add Subcard</button>
                    <button onClick={this.props.removeCard}>Delete card</button>
                </div>

                {!this.isEpic && (
                    <div className="card__controlls">
                        <div>
                            <button onClick={this.props.decreaseStart}>-</button>
                            {this.props.card.startNum}
                            <button onClick={this.props.increaseStart}>+</button>
                        </div>
                        <div>
                            <button onClick={this.props.decreaseEnd}>-</button>
                            {this.props.card.endNum}
                            <button onClick={this.props.increaseEnd}>+</button>
                        </div>
                    </div>
                )}

                {this.isEpic && (
                    <div className="card__subcards">
                        {this.props.subcards.map((subcard: ICard)=> (
                            <ConnectedCard key = {subcard.id} cardId = {subcard.id} />
                        ))}
                    </div>
                )}

            </div>

        )
    }
}

// Card.displayName = 'Card';

// const mapStateToProps = (state: IApplicationState, props: ICardProps) => ({
//     card: state.cards.find(
//         (item) => item.id === props.cardId
//     )
// });

const mapStateToProps = (state: IApplicationState, props: ICardProps) => {
    const card = state.cards.find(
        (item) =>
            item.id === props.cardId
        )
    console.dir(card);

    let subcards: ICard[] = [];

    if (card.cards.length > 0) {
        subcards = state.cards.filter((item)=> card.cards.includes(item.id));
    }

    return {
        card,
        subcards
    };
}

const mapDispatchToProps = (dispatch: any, props: ICardProps): IDispatchProps => ({

    increaseStart: () => {
        console.log('increaseStart');
        dispatch(changeNumbers(increaseStart(props.cardId)));
    },
    increaseEnd: () => dispatch(changeNumbers(increaseEnd(props.cardId))),
    decreaseStart: () => dispatch(changeNumbers(decreaseStart(props.cardId))),
    decreaseEnd: () => dispatch(changeNumbers(decreaseEnd(props.cardId))),

    removeCard: () => dispatch(removeCard(props.cardId)),
    addSubcard: () => dispatch(addCard(props.cardId, makeEmptyCardModel('newCard1', 0, 1)))
});

const ConnectedCard =
    connect<IStateProps, IDispatchProps>(mapStateToProps, mapDispatchToProps)(Card);

export default ConnectedCard;
