import * as React from 'react';
import { connect } from 'react-redux';
import { addCard, changeNumbers, increaseEnd } from '../store/actions/cardActions';
import store from '../store/configureStore';
import { makeEmptyCardModel } from '../store/helpers';

interface IStressPanelDispatchProps {
    makeThousand: () => void;
    makeMillion: () => void;
    touchLast: () => void;
    touchLast100times: () => void;
}

const StressPanel: React.SFC<IStressPanelDispatchProps> = (props) => (
    <div className="stressPanel">
        <button onClick={props.makeThousand}>makeThousand</button>
        <button onClick={props.makeMillion}>makeMillion</button>
        <button onClick={props.touchLast}>touchLast</button>
        <button onClick={props.touchLast100times}>touchLast100times</button>
    </div>
)

const mapDispatchToProps = (dispatch: any, props: any): IStressPanelDispatchProps => {
    let id;
    const propsToInject = {
        makeThousand: () => {
            let id = undefined;

            for (let times = 0; times<50; times++) {
                dispatch(addCard(id, makeEmptyCardModel(`Card ${times}`, 0, 1)));
                id = store.getState().cards.slice(-1)[0].id;
            }
        },
        makeMillion: () => {
            let id = undefined;

            for (let times = 0; times<1000000; times++) {
                dispatch(addCard(id, makeEmptyCardModel(`Card ${times}`, 0, 1)));
                id = store.getState().cards.slice(-1)[0].id;
            }
        },
        touchLast: () => {
            id = store.getState().cards.slice(-1)[0].id;
            dispatch(changeNumbers(increaseEnd(id)));
        },
        touchLast100times: () => {
            for (let times = 0; times<100; times++){
                id = store.getState().cards.slice(-1)[0].id;
                dispatch(changeNumbers(increaseEnd(id)))
            }
        }
    }
    return propsToInject;
}

const ConnectedStressPanel = connect<undefined, IStressPanelDispatchProps>(undefined, mapDispatchToProps)(StressPanel)
export default ConnectedStressPanel;
