//import 'bootstrap/dist/css/bootstrap.min.css';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as Modal from 'react-modal';
import {Provider} from 'react-redux';

import AppRouter from './routes/router';
import appStore from './store/configureStore';

const appRoot = document.getElementById('app');

Modal.setAppElement(appRoot);

ReactDOM.render(
  <Provider store={appStore}>
    <AppRouter/>
  </Provider>,
  appRoot
);
