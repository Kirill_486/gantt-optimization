import createHistory from 'history/createBrowserHistory';
import * as React from 'react';
import { Route, Router, Switch } from 'react-router-dom';

import CardsPage from '../components/CardsPage';
import NotFoundPage from '../components/NotFoundPage';
import StressPanel from '../components/StressPanel';

export const history = createHistory();

const AppRouter = () => (
  <Router history={history}>
    <div>
      <div className="container">
        <div className="row">
          <div className="col-lg-12 text-center">
            <StressPanel />
            <Switch>
              <Route path="/" component={CardsPage} exact={true} />
              <Route component={NotFoundPage} />
            </Switch>
          </div>
        </div>
      </div>
    </div>

  </Router>
);

export default AppRouter;
