export const cardsInitial = [
    {
        id: '1',
        title: 'First Card',
        startNum: 0,
        endNum: 1,
        cards: [] as string[],
        parentId: '100'
    },
    {
        id: '2',
        title: 'Second Card',
        startNum: 0,
        endNum: 1,
        parentId: '100',
        cards: [] as string[]
    },
    {
        id: '100',
        title: 'Main Card',
        startNum: 0,
        endNum: 1,
        cards: ['1', '2'],
        parentId: undefined
    }
];
