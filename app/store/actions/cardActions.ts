import { ActionCreator } from 'redux';
import { Dispatch } from 'redux';
import {ThunkAction} from 'redux-thunk';
import { IActionWithCard, IActionWithId, ICard } from '../types/types';

export const addCard: ActionCreator<IActionWithCard> = (
    id: string,
    card: ICard
    ) => ({
    type: 'ADD_CARD',
    id,
    card: card
});

export const removeCard: ActionCreator<IActionWithId> = (
    id: string
    ) => ({
    type: 'REMOVE_CARD',
    id
});

export const increaseStart: ActionCreator<IActionWithId> = (
    id: string
    ) => ({
    type: 'INC_START',
    id
});

export const decreaseStart: ActionCreator<IActionWithId> = (
    id: string
    ) => ({
    type: 'DEC_START',
    id
});

export const increaseEnd: ActionCreator<IActionWithId> = (
    id: string
    ) => ({
    type: 'INC_END',
    id
});

export const decreaseEnd: ActionCreator<IActionWithId> = (
    id: string
    ) => ({
    type: 'DEC_END',
    id
});

export const reajustNums: ActionCreator<IActionWithId> = (
    id: string
    ) => ({
    type: 'REAJUST',
    id
});

export const changeNumbers: ActionCreator<any> = (
    wrapingAction: IActionWithId
) => {
    const id = wrapingAction.id;
    const action: ThunkAction<void, ICard[], IActionWithId, any> = (dispatch: Dispatch<any>) => {
        dispatch(wrapingAction);
        dispatch(reajustNums(id));
    }
    return action;
}
