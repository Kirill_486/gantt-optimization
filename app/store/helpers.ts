import {IApplicationState, ICard, ICardModel} from './types/types';

export const changeNum = (
    id: string,
    isEnd: boolean,
    isDecrease: boolean,
    state: ICard[]
    ) => {
    const coorrectedState = state.map((item)=> {
        if (item.id === id) {
            if (!isEnd) {
                // Is start
                if (!isDecrease) {
                    // Is increase
                    item.startNum++;
                } else {
                    // Is decrease
                    item.startNum--;
                }
            } else {
                // Is end
                if (!isDecrease) {
                    // Is increase
                    item.endNum++;
                } else {
                    // Is decrease
                    item.endNum--;
                }
            }
        }
        return item;
    });
    return coorrectedState;
}

let cardModelNumber = 0;

export const makeEmptyCardModel = (
        title: string,
        startNum: number,
        endNum: number,
    ): ICardModel => ({
    title: `${title}_${cardModelNumber++}`,
    startNum,
    endNum,
    cards: [] as string[],
    parentId: undefined
});

export const canIncreaseStartNum = (card: ICard) => card.endNum > card.startNum;
export const canDecreaseEndNum = (card: ICard) => card.endNum > card.startNum;

export const getTargetById = (id: string, cards: ICard[]) => cards.find((item) => item.id === id);
