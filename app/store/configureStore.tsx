import { combineReducers, createStore } from 'redux';
import { applyMiddleware } from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import thunk from 'redux-thunk';

import cardsReducer from './reducers/cardReducer';

// temporaly inputs
// import {addCard, removeCard, increaseStart, increaseEnd, decreaseStart, decreaseEnd} from './actions/cardActions'
// import {makeEmptyCardModel} from './helpers'

const store = createStore(combineReducers({
    cards: cardsReducer
}), composeWithDevTools(applyMiddleware(thunk)));

export default store;

export const unsubscribe = store.subscribe(() => {
    console.log(JSON.stringify(store.getState(), undefined, 2));
  });

console.log(`Initial state ${JSON.stringify(store.getState(), undefined, 2)}`);

// //add card to root
// console.log('--');
// console.log('new root card');
// store.dispatch(addCard(null, makeEmptyCardModel('newCard1', 0, 1)));

// //add subcard
// console.log('--');
// console.log('add subcards');
// store.dispatch(addCard("1", makeEmptyCardModel('newCard1', 0, 1)));
// store.dispatch(addCard("1", makeEmptyCardModel('newCard2', 0, 1)));
// store.dispatch(addCard("2", makeEmptyCardModel('newCard3', 0, 1)));

// console.log('--');
// console.log('changeNums of card 1');
// store.dispatch(increaseStart("1"));
// store.dispatch(increaseStart("1"));
// store.dispatch(increaseStart("1"));

// console.log('--');

// store.dispatch(increaseEnd("1"));
// store.dispatch(increaseEnd("1"));
// store.dispatch(increaseEnd("1"));

// console.log('--');

// store.dispatch(decreaseStart("1"));
// store.dispatch(decreaseStart("1"));

// console.log('--');

// store.dispatch(decreaseEnd("1"));
// store.dispatch(decreaseEnd("1"));

// console.log('--');
// console.log('remove subcards');

// //remove cards
// store.dispatch(removeCard("1"));
// store.dispatch(removeCard("2"));
// store.dispatch(removeCard("100"));
