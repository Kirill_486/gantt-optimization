import * as uuid from 'uuid';

import {cardsInitial} from '../defaults';
import {canDecreaseEndNum, canIncreaseStartNum, changeNum, getTargetById} from '../helpers';
import { IActionWithCard, ICard, TApplicationAction } from '../types/types';

const cardsReducer = (state: ICard[] = cardsInitial, action: TApplicationAction) => {

    let correctedState = [];
    let target;

    switch (action.type) {
        case 'ADD_CARD':

            const cardAction: IActionWithCard = action as IActionWithCard;

            //new cards id
            cardAction.card.id = uuid();
            cardAction.card.parentId = action.id;

            //add a card to parent card
            correctedState = state.map((item)=> {
                if (item.id === action.id) {
                    item.cards = [...item.cards, cardAction.card.id]
                }
                return item;
            });

            //add new card to state
            return [...correctedState, cardAction.card];

        case 'REMOVE_CARD':
            correctedState = state.reduce((accumulator, item)=> {
                // remove card from state
                if (item.id !== action.id) {
                    accumulator = [...accumulator, item]
                }

                // remove card from children
                if (item.cards) {
                    item.cards = item.cards.filter((id)=>id !== action.id);
                }

                return accumulator;
            }, []);

            return correctedState;

        case 'INC_START':
            target = getTargetById(action.id, state);
            if (canIncreaseStartNum(target)) {
                return changeNum(action.id, false, false, state);
            } else {
                return state;
            }
        case 'DEC_START':
            return changeNum(action.id, false, true, state);
        case 'INC_END':
            return changeNum(action.id, true, false, state);
        case 'DEC_END':
            target = getTargetById(action.id, state);
            if (canDecreaseEndNum(target)) {
                return changeNum(action.id, true, true, state);
            } else {
                return state;
            }
        case 'REAJUST' :
            target = getTargetById(action.id, state);
            const newBiggestEndNum = target.endNum;
            const newSmallestStartNum = target.startNum;

            const relativesIds = [] as string[];
            while (target.parentId) {
                relativesIds.push(target.parentId);
                target = getTargetById(target.parentId, state);
            }

            return state.map((item)=>{
                if (relativesIds.includes(item.id)){

                    if (item.startNum > newSmallestStartNum) {
                        item.startNum = newSmallestStartNum;
                    }

                    if (item.endNum  < newBiggestEndNum) {
                        item.endNum = newBiggestEndNum;
                    }
                }

                return item;
            });
        default: return state;
    }
}

export default cardsReducer;