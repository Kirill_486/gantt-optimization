import { Action } from 'redux';

export interface ICardModel {
    title: string;
    startNum: number;
    endNum: number;
    cards: string[];
    parentId: string;
}

export interface ICard extends ICardModel {
    id: string;
}

export interface IApplicationState {
    cards: ICard[]
}

export interface IActionWithId extends Action {
    id: string
}

export interface IActionWithCard extends IActionWithId {
    card: ICard
}

export type TApplicationAction = IActionWithId | IActionWithCard;
